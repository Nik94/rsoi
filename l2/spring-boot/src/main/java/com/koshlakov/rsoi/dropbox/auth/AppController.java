package com.koshlakov.rsoi.dropbox.auth;

import com.koshlakov.rsoi.dropbox.auth.config.Good;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class AppController {

    private static final String REFRESH_URL = "http://localhost:8080/refresh";

    @RequestMapping(value = "/billing", method = RequestMethod.GET)
    public ModelAndView billing(HttpServletRequest httpServletRequest) {
        String orderId = httpServletRequest.getParameter("orderId");
        String goodId = httpServletRequest.getParameter("goodId");
        String url = "http://localhost:8080/orders/" + orderId + "/billing";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());

        HttpEntity request = new HttpEntity(headers);

        Object response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                refreshToken();
                headers = new HttpHeaders();
                headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());
                request = new HttpEntity(headers);
                response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
            }
        }

        ModelAndView modelAndView = new ModelAndView("redirect:" + "/viewOrder");
        modelAndView.addObject("orderId", orderId);
        return modelAndView;
    }

    @RequestMapping(value = "/deleteGood", method = RequestMethod.GET)
    public ModelAndView deleteGood(HttpServletRequest httpServletRequest) {
        String orderId = httpServletRequest.getParameter("orderId");
        String goodId = httpServletRequest.getParameter("goodId");
        String url = "http://localhost:8080/orders/" + orderId + "/goods/" + goodId;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());

        HttpEntity request = new HttpEntity(headers);

        Object response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.DELETE, request, Object.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                refreshToken();
                headers = new HttpHeaders();
                headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());
                request = new HttpEntity(headers);
                response = restTemplate.exchange(url, HttpMethod.DELETE, request, Object.class);
            }
        }

        ModelAndView modelAndView = new ModelAndView("redirect:" + "/viewOrder");
        modelAndView.addObject("orderId", orderId);
        return modelAndView;
    }

    @RequestMapping(value = "/addGood", method = RequestMethod.GET)
    public ModelAndView addGood(HttpServletRequest httpServletRequest) {
        String orderId = httpServletRequest.getParameter("orderId");
        String goodId = httpServletRequest.getParameter("goodId");
        String url = "http://localhost:8080/orders/" + orderId + "/goods/" + goodId;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());

        HttpEntity request = new HttpEntity(headers);

        Object response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                refreshToken();
                headers = new HttpHeaders();
                headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());
                request = new HttpEntity(headers);
                response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
            }
        }

        ModelAndView modelAndView = new ModelAndView("redirect:" + "/viewOrder");
        modelAndView.addObject("orderId", orderId);
        return modelAndView;
    }

    @RequestMapping(value = "/viewOrder", method = RequestMethod.GET)
    public ModelAndView viewOrder(HttpServletRequest httpServletRequest) {
        String orderId = httpServletRequest.getParameter("orderId");
        String url = "http://localhost:8080/orders/" + orderId;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());

        HttpEntity request = new HttpEntity(headers);

        Object response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                refreshToken();
                headers = new HttpHeaders();
                headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());
                request = new HttpEntity(headers);
                response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
            }
        }

        Map order = (Map) ((ResponseEntity)response).getBody();
        ModelAndView modelAndView = new ModelAndView("order");
        modelAndView.addObject("orderId", order.get("id"));
        modelAndView.addObject("cost", order.get("cost"));
        List<Map> goods = (List) order.get("goods");
        List<Good> goodsForView = new ArrayList<Good>();
        if (goods != null) {
            for (Map good : goods) {
                Good g = new Good();
                g.setId(((Integer) good.get("id")).toString());
                g.setName((String) good.get("name"));
                g.setCost(((Integer) good.get("cost")).toString());
                goodsForView.add(g);
            }
        }
        modelAndView.addObject("goods", goodsForView);
        return modelAndView;
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public ModelAndView orders(HttpServletRequest httpServletRequest) {
        String url = "http://localhost:8080/orders";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());

        HttpEntity request = new HttpEntity(headers);

        Object response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                refreshToken();
                headers = new HttpHeaders();
                headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());
                request = new HttpEntity(headers);
                response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
            }
        }

        List<Map> orders = (List) ((ResponseEntity)response).getBody();
        List ids = new ArrayList();
        for (Map order : orders) {
            ids.add(order.get("id"));
        }
        ModelAndView modelAndView = new ModelAndView("orders");
        modelAndView.addObject("orders", ids);
        return modelAndView;
    }

    @RequestMapping(value = "/newOrder", method = RequestMethod.GET)
    public ModelAndView newOrder(HttpServletRequest httpServletRequest) {
        String url = "http://localhost:8080/orders";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());

        HttpEntity request = new HttpEntity(headers);

        Object response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                refreshToken();
                headers = new HttpHeaders();
                headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());
                request = new HttpEntity(headers);
                response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
            }
        }

        return new ModelAndView("redirect:" + "/orders");
    }

    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public ModelAndView me(HttpServletRequest httpServletRequest) {
        String url = "http://localhost:8080/me";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());

        HttpEntity request = new HttpEntity(headers);

        Object response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
                refreshToken();
                headers = new HttpHeaders();
                headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getToken());
                request = new HttpEntity(headers);
                response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
            }
        }

        Map map = (Map)((ResponseEntity)response).getBody();
        ModelAndView modelAndView = new ModelAndView("hello");
        modelAndView.addObject("name", map.get("name"));
        return modelAndView;
    }

    private void refreshToken() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, OAuth2Helper.getRefresh_token());

        HttpEntity request = new HttpEntity(headers);

        Object response = restTemplate.exchange(REFRESH_URL, HttpMethod.GET, request, Object.class);

        Map map = (Map)((ResponseEntity)response).getBody();
        OAuth2Helper.setToken((String)map.get("token"));
        OAuth2Helper.setRefresh_token((String)map.get("refresh_token"));
    }
}
