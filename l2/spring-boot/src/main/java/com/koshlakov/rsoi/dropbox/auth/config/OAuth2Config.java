package com.koshlakov.rsoi.dropbox.auth.config;

public class OAuth2Config {
    private static volatile OAuth2Config instance;
    private final String REDIRECT_URL = "http://localhost:8191/app";
    private final String APP_KEY = "qr7hvm829lmvnvo";
    private final String SECRET_KEY = "f59kovp39koupx6";

    public String getRedirectUrl() {
        return REDIRECT_URL;
    }

    public String getAppKey() {
        return APP_KEY;
    }

    public String getSecretKey() {
        return SECRET_KEY;
    }

    public static OAuth2Config getInstance() {
        OAuth2Config localInstance = instance;
        if (localInstance == null) {
            synchronized (OAuth2Config.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new OAuth2Config();
                }
            }
        }
        return localInstance;
    }
}
