package com.koshlakov.rsoi.dropbox.auth;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

@RestController
public class HelloController {

    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
        return new ModelAndView("redirect:" + new OAuth2Helper()
//                .buildRedirectUrl(((CsrfToken) request.getAttribute("_csrf")).getToken()));
                .buildRedirectUrl(null));
    }

    @RequestMapping(value = "/app", method = RequestMethod.GET)
    public ModelAndView index(@RequestParam("code") String code,
                              @RequestParam(required=false, value="state") String state) {

        OAuth2Helper helper = new OAuth2Helper();
        String accessToken = null;
        try {
            accessToken = helper.getAccessToken(code, state);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
        String info = null;
//        try {
//            info = helper.getAccountInfo(accessToken);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        JsonObject obj = new JsonParser().parse(info).getAsJsonObject();
//        String access = obj.get("display_name").getAsString();
//        return new String("Hello," + access + "!");
        return new ModelAndView("redirect:" + "/me");
    }

}
