package com.koshlakov.rsoi.rest.api;

import com.koshlakov.rsoi.rest.api.exception.Messages;
import com.koshlakov.rsoi.rest.api.pojos.oauth.Token;
import com.koshlakov.rsoi.rest.api.pojos.oauth.VerificationCode;
import com.koshlakov.rsoi.utils.DBUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class ApiEntry {

    private static final String CORRECT_RESPONSE_TYPE = "code";
    private static final String CORRECT_TOKEN_TYPE = "authorization_code";
    private final String APP_KEY = "qr7hvm829lmvnvo";
    private final String SECRET_KEY = "f59kovp39koupx6";

    private static String redirectUriStatic = null;
    private static String csrfStatic = null;

    @RequestMapping(value = "/oauth2/authorize", method = RequestMethod.GET)
    public ModelAndView getVerificationCode(@RequestParam("client_id") String client_id,
                                     @RequestParam("response_type") String responseType,
                                     @RequestParam("redirect_uri") String redirect_uri,
                                     @RequestParam(required=false, value="state") String csrf) {
        if (!CORRECT_RESPONSE_TYPE.equals(responseType)) {
            throw Messages.INCORRECT_RESPONSE_CODE;
        }
        if (!APP_KEY.equals(client_id)) {
            throw Messages.INCORRECT_CLIENT_ID;
        }

        redirectUriStatic = redirect_uri;

        return new ModelAndView("login");
    }

    @RequestMapping(value = "/authorize", method = RequestMethod.POST)
    public ModelAndView authorize(@RequestParam String username,
                          @RequestParam String password) {
        VerificationCode code = DBUtils.getInstance().createVerificationCode(username, password);
        return new ModelAndView("redirect:" + redirectUriStatic + "?code=" + code.getVerificationCode());
    }

    @RequestMapping(value = "/oauth2/token", method = RequestMethod.POST)
    public Token getToken(@RequestParam String grant_type,
                          @RequestParam String code,
                          @RequestParam String client_id,
                          @RequestParam String client_secret,
                          @RequestParam String redirect_uri) {
        if (!"authorization_code".equals(grant_type)) {
            throw Messages.INCORRECT_GRANT_TYPE;
        }

        if (!APP_KEY.equals(client_id)) {
            throw Messages.INCORRECT_CLIENT_ID;
        }

        if (!SECRET_KEY.equals(client_secret)) {
            throw Messages.INCORRECT_CLIENT_SECRET;
        }

        Token token = DBUtils.getInstance().createToken(code);

        //return new ModelAndView("redirect:" + redirect_uri + "?token=" + token.getToken());
//        return Response
//                .ok(token, MediaType.APPLICATION_JSON_TYPE)
//                .header(HttpHeaders.ACCEPT, "*/*")
//                .build();
        return token;
    }
}
