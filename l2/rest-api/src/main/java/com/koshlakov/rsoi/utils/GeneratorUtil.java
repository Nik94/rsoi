package com.koshlakov.rsoi.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;


public enum GeneratorUtil {
    INSTANCE;

    private SecureRandom random = new SecureRandom();

    public String getToken() {
        return new BigInteger(130, random).toString(32);
    }

    public String getVerificationCode() {
        int MAX_RAND = 100000;
        Random randomGenerator = new Random();
        return new Integer(randomGenerator.nextInt(MAX_RAND)).toString();
    }

    public String getHash(final String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] bytesOfMessage = password.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] thedigest = md.digest(bytesOfMessage);
        return new String(thedigest);
    }
}
