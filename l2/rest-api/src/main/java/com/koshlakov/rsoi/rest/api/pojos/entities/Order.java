package com.koshlakov.rsoi.rest.api.pojos.entities;

import com.koshlakov.rsoi.rest.api.pojos.oauth.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.OptimisticLockType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "orders", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")})
public class Order implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonProperty("id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name="userid")
    @JsonIgnore
    private User user;

    @Transient
    @JsonProperty("user")
    private String userName;

    @Transient
    @JsonProperty("goods")
    private List<Good> goods;

    @Transient
    @JsonProperty("cost")
    private Integer billing;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userName = user.getUsername();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }

    public Integer getBilling() {
        return billing;
    }

    public void setBilling(Integer billing) {
        this.billing = billing;
    }
}
