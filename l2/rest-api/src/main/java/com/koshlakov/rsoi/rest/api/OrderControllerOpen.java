package com.koshlakov.rsoi.rest.api;

import com.koshlakov.rsoi.rest.api.pojos.entities.Good;
import com.koshlakov.rsoi.utils.DBUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderControllerOpen {

    @RequestMapping(value = "/goods", method = RequestMethod.GET)
    public List<Good> getGoods(@RequestHeader("Range") String range) {
        String[] tokens = range.split("-");
        return DBUtils.getInstance().getGoods(new Integer(tokens[0]), new Integer(tokens[1]));
    }

    @RequestMapping(value = "/goods/{id}", method = RequestMethod.GET)
    public Good getGood(@PathVariable("id") String id) {
        return DBUtils.getInstance().getGoog(new Integer(id));
    }
}
