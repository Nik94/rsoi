package com.koshlakov.rsoi.utils;

import com.koshlakov.rsoi.HibernateUtil;
import com.koshlakov.rsoi.rest.api.pojos.entities.Billing;
import com.koshlakov.rsoi.rest.api.pojos.entities.Good;
import com.koshlakov.rsoi.rest.api.pojos.entities.Goods2Orders;
import com.koshlakov.rsoi.rest.api.pojos.oauth.Token;
import com.koshlakov.rsoi.rest.api.pojos.oauth.User;
import com.koshlakov.rsoi.rest.api.pojos.oauth.VerificationCode;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DBUtils {
    private static volatile DBUtils instance;

    public static DBUtils getInstance() {
        DBUtils localInstance = instance;
        if (localInstance == null) {
            synchronized (DBUtils.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DBUtils();
                }
            }
        }
        return localInstance;
    }

    public boolean isTokenExpired(String token) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Token.class);
        criteria.add(Restrictions.eq("token", token));
        Token tokenObj = (Token) criteria.uniqueResult();
        HibernateUtil.shutdown();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        if (tokenObj.getExpirationDate().before(calendar.getTime())) {
            return true;
        }
        return false;
    }

    public com.koshlakov.rsoi.rest.api.pojos.entities.Order createOrder(String token) {
        User user = getUserByToken(token);
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        com.koshlakov.rsoi.rest.api.pojos.entities.Order order = new com.koshlakov.rsoi.rest.api.pojos.entities.Order();
        order.setUser(user);

        session.save(order);

        session.getTransaction().commit();
        HibernateUtil.shutdown();
        return order;
    }

    public com.koshlakov.rsoi.rest.api.pojos.entities.Order getOrder(String token, Integer id) {
        User user = getUserByToken(token);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(com.koshlakov.rsoi.rest.api.pojos.entities.Order.class);
        criteria.add(Restrictions.eq("id", id));
        criteria.add(Restrictions.eq("user", user));
        com.koshlakov.rsoi.rest.api.pojos.entities.Order order =
                (com.koshlakov.rsoi.rest.api.pojos.entities.Order) criteria.uniqueResult();
        order.setUserName(order.getUser().getUsername());

        Criteria criteria1 = session.createCriteria(Billing.class);
        criteria1.add(Restrictions.eq("id", order.getId()));
        Billing billing = (Billing)criteria1.uniqueResult();
        if (billing != null) {
            order.setBilling(billing.getCost());
        }
        Criteria criteria2 = session.createCriteria(Goods2Orders.class);
        criteria2.add(Restrictions.eq("order", order.getId()));

        List<Goods2Orders> goodToOrders = criteria2.list();
        List<Integer> goodIds = new ArrayList<>();
        for (Goods2Orders goods2Orders : goodToOrders) {
            goodIds.add(goods2Orders.getGood());
        }

        if (!goodIds.isEmpty()) {
            Criteria criteria3 = session.createCriteria(Good.class);
            criteria3.add(Restrictions.in("id", goodIds));
            List<Good> goods = criteria3.list();

            order.setGoods(goods);
        }
        HibernateUtil.shutdown();

        return order;
    }

    public List<com.koshlakov.rsoi.rest.api.pojos.entities.Order> getOrders(String token) {
        User user = getUserByToken(token);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(com.koshlakov.rsoi.rest.api.pojos.entities.Order.class);
        criteria.add(Restrictions.eq("user", user));
        List<com.koshlakov.rsoi.rest.api.pojos.entities.Order> orders = criteria.list();
        for (com.koshlakov.rsoi.rest.api.pojos.entities.Order order : orders) {
            order.setUserName(order.getUser().getUsername());

            Criteria criteria1 = session.createCriteria(Billing.class);
            criteria1.add(Restrictions.eq("id", order.getId()));
            Billing billing = (Billing)criteria1.uniqueResult();
            if (billing != null) {
                order.setBilling(billing.getCost());
            }

            Criteria criteria2 = session.createCriteria(Goods2Orders.class);
            criteria2.add(Restrictions.eq("order", order.getId()));

            List<Goods2Orders> goodToOrders = criteria2.list();
            List<Integer> goodIds = new ArrayList<>();
            for (Goods2Orders goods2Orders : goodToOrders) {
                goodIds.add(goods2Orders.getGood());
            }

            if (!goodIds.isEmpty()) {
                Criteria criteria3 = session.createCriteria(Good.class);
                criteria3.add(Restrictions.in("id", goodIds));
                List<Good> goods = criteria3.list();

                order.setGoods(goods);
            }
        }
        HibernateUtil.shutdown();
        return orders;
    }


    public void addGoodToOrder(Integer orderId, Integer goodId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(com.koshlakov.rsoi.rest.api.pojos.entities.Order.class);
        criteria.add(Restrictions.eq("id", orderId));
        com.koshlakov.rsoi.rest.api.pojos.entities.Order order =
                (com.koshlakov.rsoi.rest.api.pojos.entities.Order) criteria.uniqueResult();

        Criteria criteria1 = session.createCriteria(Good.class);
        criteria1.add(Restrictions.eq("id", goodId));
        Good good = (Good) criteria1.uniqueResult();

        session.beginTransaction();

        Goods2Orders goods2Orders = new Goods2Orders();
        goods2Orders.setOrder(order.getId());
        goods2Orders.setGood(good.getId());

        session.save(goods2Orders);

        session.getTransaction().commit();
        HibernateUtil.shutdown();
    }

    public void deleteGoodFromOrder(Integer orderId, Integer goodId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(com.koshlakov.rsoi.rest.api.pojos.entities.Order.class);
        criteria.add(Restrictions.eq("id", orderId));
        com.koshlakov.rsoi.rest.api.pojos.entities.Order order =
                (com.koshlakov.rsoi.rest.api.pojos.entities.Order) criteria.uniqueResult();

        Criteria criteria1 = session.createCriteria(Good.class);
        criteria1.add(Restrictions.eq("id", goodId));
        Good good = (Good) criteria1.uniqueResult();

        Criteria criteria2 = session.createCriteria(Goods2Orders.class);
        criteria2.add(Restrictions.eq("order", order.getId()));
        criteria2.add(Restrictions.eq("good", good.getId()));
        Goods2Orders goods2Orders = (Goods2Orders) criteria2.uniqueResult();

        session.beginTransaction();

        session.delete(goods2Orders);

        session.getTransaction().commit();
        HibernateUtil.shutdown();
    }

    public Billing createBilling(Integer orderId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(com.koshlakov.rsoi.rest.api.pojos.entities.Order.class);
        criteria.add(Restrictions.eq("id", orderId));
        com.koshlakov.rsoi.rest.api.pojos.entities.Order order =
                (com.koshlakov.rsoi.rest.api.pojos.entities.Order) criteria.uniqueResult();

        Criteria criteria1 = session.createCriteria(Goods2Orders.class);
        criteria1.add(Restrictions.eq("order", order.getId()));
        List<Goods2Orders> goods2Orders = criteria1.list();

        Integer cost = 0;
        for (Goods2Orders goods2Order : goods2Orders) {
            Integer id = goods2Order.getGood();
            Criteria criteria2 = session.createCriteria(Good.class);
            criteria2.add(Restrictions.eq("id", id));
            Good good = (Good) criteria2.uniqueResult();
            cost += good.getCost();
        }

        session.beginTransaction();

        Billing billing = new Billing();
        billing.setId(order.getId());
        billing.setCost(cost);

        session.saveOrUpdate(billing);

        session.getTransaction().commit();
        HibernateUtil.shutdown();

        return billing;
    }

    public User getUserByToken(String token) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Token.class);
        criteria.add(Restrictions.eq("token", token));
        Token tokenObj = (Token) criteria.uniqueResult();
        Criteria criteria1 = session.createCriteria(User.class);
        criteria1.add(Restrictions.eq("id", tokenObj.getUser()));
        User user = (User) criteria1.uniqueResult();
        return user;
    }

    public void createUser(String userName, String password, String description) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = new User();

        user.setUsername(userName);
        user.setPasswordHash(GeneratorUtil.INSTANCE.getHash(password));
        user.setDescription(description);

        session.save(user);

        session.getTransaction().commit();
        HibernateUtil.shutdown();
    }

    private User getUser(String userName, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;
        try {
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("username", userName));
            criteria.add(Restrictions.eq("passwordHash", GeneratorUtil.INSTANCE.getHash(password)));
            user = (User) criteria.uniqueResult();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HibernateUtil.shutdown();

        return user;
    }

    public VerificationCode createVerificationCode(String userName, String password) {
        User user = getUser(userName, password);

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        VerificationCode code = new VerificationCode();
        code.setId(user.getId());
        code.setVerificationCode(GeneratorUtil.INSTANCE.getVerificationCode());

        session.saveOrUpdate(code);

        session.getTransaction().commit();
        HibernateUtil.shutdown();

        return code;
    }

    public Token createToken(String code) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(VerificationCode.class);
        criteria.add(Restrictions.eq("verificationCode", code));
        VerificationCode codeObj = (VerificationCode) criteria.uniqueResult();

        Token token = new Token();
        token.setUser(codeObj.getId());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, Token.EXPIRATION_TIME);
        token.setExpirationDate(calendar.getTime());
        token.setToken(GeneratorUtil.INSTANCE.getToken());
        token.setRefreshToken(GeneratorUtil.INSTANCE.getToken());

        session.saveOrUpdate(token);

        session.getTransaction().commit();
        HibernateUtil.shutdown();

        return token;
    }

    public Token refreshToken(String refreshToken) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Token.class);
        criteria.add(Restrictions.eq("refreshToken", refreshToken));
        Token token = (Token) criteria.uniqueResult();

        session.delete(token);

        session.getTransaction().commit();
        HibernateUtil.shutdown();

        Session session2 = HibernateUtil.getSessionFactory().openSession();
        session2.beginTransaction();

        Token newToken = new Token();
        newToken.setUser(token.getUser());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, Token.EXPIRATION_TIME);
        newToken.setExpirationDate(calendar.getTime());
        newToken.setToken(GeneratorUtil.INSTANCE.getToken());
        newToken.setRefreshToken(GeneratorUtil.INSTANCE.getToken());

        session2.saveOrUpdate(newToken);

        session2.getTransaction().commit();
        HibernateUtil.shutdown();

        return newToken;
    }

    public Good getGoog(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Good good = (Good) session.get(Good.class, id);
        HibernateUtil.shutdown();
        return good;
    }

    public List<Good> getGoods(Integer from, Integer to) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Good.class);
        criteria.add(Expression.ge("id", from));
        criteria.add(Expression.le("id", to));
        criteria.addOrder(Order.asc("id"));
        List list = criteria.list();
        HibernateUtil.shutdown();
        return list;
    }
}
