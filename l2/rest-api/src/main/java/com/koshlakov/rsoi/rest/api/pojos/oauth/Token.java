package com.koshlakov.rsoi.rest.api.pojos.oauth;

import com.koshlakov.rsoi.utils.DateToLongTimestampSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.OptimisticLockType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "token")
public class Token implements Serializable {
    public static final int EXPIRATION_TIME = 80;

    @Id
    @Column(name="id")
    @JsonIgnore
    private Integer user;

    @Column(name = "token", nullable = false, length = 100)
    @JsonProperty("token")
    private String token;

    @Column(name = "refresh_token", nullable = false, length = 100)
    @JsonProperty("refresh_token")
    private String refreshToken;

    @Column(name = "expiration_date")
    @JsonProperty("expirationDate")
    @JsonSerialize(using = DateToLongTimestampSerializer.class)
    private Date expirationDate;

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }
}
