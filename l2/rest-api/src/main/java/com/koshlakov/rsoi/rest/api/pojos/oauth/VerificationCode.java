package com.koshlakov.rsoi.rest.api.pojos.oauth;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.OptimisticLockType;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "verification")
public class VerificationCode implements Serializable {
    @Id
    @Column(name = "id")
    @JsonProperty("userId")
    private Integer id;

    @Column(name = "ver_code", nullable = false, length = 100)
    @JsonProperty("verificationCode")
    private String verificationCode;

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
