package com.koshlakov.rsoi.rest.api.pojos.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.OptimisticLockType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "billings", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")})
public class Billing implements Serializable {

    @Id
    @JsonIgnore
    private Integer id;

    @Column(name = "cost", nullable = false)
    @JsonProperty("cost")
    private Integer cost;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
