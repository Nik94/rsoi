package com.koshlakov.rsoi.rest.api.exception;

public class ApiException extends RuntimeException {

    public ApiException(String message) {
        super(message);
    }
}
