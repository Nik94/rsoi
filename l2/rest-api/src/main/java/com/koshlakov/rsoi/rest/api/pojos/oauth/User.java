package com.koshlakov.rsoi.rest.api.pojos.oauth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.OptimisticLockType;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")})
@Embeddable
public class User implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonIgnore
    private Integer id;

    @Column(name = "username", unique = true, nullable = false, length = 100)
    @JsonProperty("name")
    private String username;

    @Column(name = "passwordhash", nullable = false, length = 100)
    @JsonIgnore
    private String passwordHash;

    @Column(name = "description", length = 100)
    @JsonProperty("description")
    private String description;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
