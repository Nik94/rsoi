package com.koshlakov.rsoi.rest.api;

import com.koshlakov.rsoi.utils.DBUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

@RestController
public class CreateUser {
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Response addUser(@RequestParam String userName,
                            @RequestParam String password,
                            @RequestParam(required = false) String description) {
        try {
            DBUtils.getInstance().createUser(userName, password, description);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.status(Response.Status.CREATED).build();
    }
}
