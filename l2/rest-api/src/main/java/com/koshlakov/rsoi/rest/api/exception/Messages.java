package com.koshlakov.rsoi.rest.api.exception;

public interface Messages {

    ApiException INCORRECT_RESPONSE_CODE = new ApiException("Incorrect responseType");

    ApiException INCORRECT_CLIENT_ID = new ApiException("Incorrect client ID");

    ApiException INCORRECT_CLIENT_SECRET = new ApiException("Incorrect clien secret");

    ApiException INCORRECT_GRANT_TYPE = new ApiException("Incorrect grantType");

    ApiException VERIFICATION_CODE_EXPIRED = new ApiException("Verification code expired");
}
