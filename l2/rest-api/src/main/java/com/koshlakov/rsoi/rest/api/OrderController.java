package com.koshlakov.rsoi.rest.api;

import com.koshlakov.rsoi.utils.DBUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.HttpHeaders;


@RestController
public class OrderController {

    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    public ResponseEntity refresh(@RequestHeader(HttpHeaders.AUTHORIZATION) String refreshToken) {
        return new ResponseEntity(DBUtils.getInstance().refreshToken(refreshToken), HttpStatus.OK);
    }

    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public ResponseEntity getUser(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        if (DBUtils.getInstance().isTokenExpired(token)) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity(DBUtils.getInstance().getUserByToken(token), HttpStatus.OK);
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public ResponseEntity getOrders(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        if (DBUtils.getInstance().isTokenExpired(token)) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity(DBUtils.getInstance().getOrders(token), HttpStatus.OK);
    }

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET)
    public ResponseEntity getOrder(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                         @PathVariable("id") String id) {
        if (DBUtils.getInstance().isTokenExpired(token)) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity(DBUtils.getInstance().getOrder(token, new Integer(id)), HttpStatus.OK);
    }

    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    public ResponseEntity createOrder(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        if (DBUtils.getInstance().isTokenExpired(token)) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity(DBUtils.getInstance().createOrder(token), HttpStatus.CREATED);
    }

//    @RequestMapping(value = "/orders/{id}", method = RequestMethod.PUT)
//    public void editOrder() {
//
//    }

    @RequestMapping(value = "/orders/{orderId}/goods/{goodId}", method = RequestMethod.POST)
    public ResponseEntity addGoodToOrder(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                               @PathVariable("orderId") String orderId,
                               @PathVariable("goodId") String goodId) {
        if (DBUtils.getInstance().isTokenExpired(token)) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        DBUtils.getInstance().addGoodToOrder(new Integer(orderId), new Integer(goodId));
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/orders/{orderId}/goods/{goodId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteGoodFromOrder(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                    @PathVariable("orderId") String orderId,
                                    @PathVariable("goodId") String goodId) {
        DBUtils.getInstance().deleteGoodFromOrder(new Integer(orderId), new Integer(goodId));
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/orders/{id}/billing", method = RequestMethod.POST)
    public ResponseEntity createBilling(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                 @PathVariable("id") String id) {
        if (DBUtils.getInstance().isTokenExpired(token)) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity(DBUtils.getInstance().createBilling(new Integer(id)),HttpStatus.OK);
    }
}
