package com.koshlakov.rsoi.auth;


public class InternalAuth {
    private static volatile InternalAuth instance;
    private final String USERNAME = "user";
    private final String PASSWORD = "password";
    private final String ROLE = "USER";

    public String getUsername() {
        return USERNAME;
    }

    public String getPassword() {
        return PASSWORD;
    }

    public String getRole() {
        return ROLE;
    }

    public static InternalAuth getInstance() {
        InternalAuth localInstance = instance;
        if (localInstance == null) {
            synchronized (InternalAuth.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new InternalAuth();
                }
            }
        }
        return localInstance;
    }
}
