package com.koshlakov.rsoi.dropbox;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@RestController
public class HelloController {

    @RequestMapping(value = "/authdropbox", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
        return new ModelAndView("redirect:" + new OAuth2Helper()
                .buildRedirectUrl(((CsrfToken) request.getAttribute("_csrf")).getToken()));
    }

    @RequestMapping(value = "/dropbox", method = RequestMethod.GET)
    public String index(@RequestParam("code") String code,
                              @RequestParam("state") String state) {

        OAuth2Helper helper = new OAuth2Helper();
        String accessToken = null;
        try {
            accessToken = helper.getAccessToken(code, state);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
        String info = null;
        try {
            info = helper.getAccountInfo(accessToken);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonObject obj = new JsonParser().parse(info).getAsJsonObject();
        String access = obj.get("display_name").getAsString();
        JsonElement email = obj.get("email");
        JsonElement uid = obj.get("uid");
        JsonElement country = obj.get("country");
        return new String("Hello, " + access + "")
                + new String("\nSome information:\n"
                + "email: " + email.toString()
                + "\nuser id: " + uid.toString()
                + "\ncountry: " + country.toString());
    }

}
