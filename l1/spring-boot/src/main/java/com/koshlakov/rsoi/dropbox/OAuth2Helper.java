package com.koshlakov.rsoi.dropbox;

import com.koshlakov.rsoi.config.auth.OAuth2Config;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.log4j.Logger;

import javax.naming.AuthenticationException;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpHeaders.USER_AGENT;


public class OAuth2Helper {

    private Logger LOGGER = Logger.getLogger(OAuth2Helper.class);

    private static String csfrOriginal;

    public String buildRedirectUrl(String csfr) {
        LOGGER.info("Build redirect URL to Dropbox Authorization page");
        String redirectUrl = "http://localhost:8080/dropbox";
        StringBuilder projectUrl = new StringBuilder();
        projectUrl.append("https://www.dropbox.com/1/oauth2/authorize?client_id=");
        projectUrl.append(OAuth2Config.getInstance().getAppKey());
        projectUrl.append("&response_type=code&redirect_uri=");
        projectUrl.append(redirectUrl);
        projectUrl.append("&state=");
        projectUrl.append(csfr);
        csfrOriginal = csfr;
        LOGGER.info("Redirect URL = " + projectUrl.toString());
        return projectUrl.toString();
    }

    public String getAccessToken(String code, String state) throws AuthenticationException{
        if (!(csfrOriginal != null && csfrOriginal.equals(state))) {
            LOGGER.info("Csfr tokens are different!");
            throw new AuthenticationException();
        }

        String result = null;
        try {
            result = sendPostForAccessToken(code);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonObject obj = new JsonParser().parse(result).getAsJsonObject();
        String access = obj.get("access_token").getAsString();
        return access;
    }

    private String sendPostForAccessToken(String code) throws IOException {
        LOGGER.info("Build request and send POST for access token");
        LOGGER.info("Code = " + code);
        String url = "https://api.dropboxapi.com/1/oauth2/token";
        HttpsURLConnection con = null;
        try {
            con = (HttpsURLConnection) new URL(url).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            con.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = buildUrlParametersForGetAccessToken(code);
        LOGGER.info("Send request to URL = " + url + " with parameters = " + urlParameters);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        LOGGER.info("Response = " + response.toString());
        return response.toString();
    }

    private String buildUrlParametersForGetAccessToken(String code) {
        String redirectUrl = "http://localhost:8080/dropbox";

        Map<String, String> params = new HashMap<String, String>();
        params.put("code", code);
        params.put("grant_type", "authorization_code");
        params.put("client_id", OAuth2Config.getInstance().getAppKey());
        params.put("client_secret", OAuth2Config.getInstance().getSecretKey());
        params.put("redirect_uri", redirectUrl);

        StringBuilder stringBuilder = new StringBuilder();
        for (String key: params.keySet()) {
            stringBuilder.append(key + "=" + params.get(key) + "&");
        }
        stringBuilder.deleteCharAt(stringBuilder.length()-1);

        return stringBuilder.toString();
    }

    public String getAccountInfo(String access) throws IOException {
        LOGGER.info("Get Account info from Dropbox");
        String url = "https://api.dropbox.com/1/account/info";
        HttpsURLConnection con = null;
        try {
            con = (HttpsURLConnection) new URL(url).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            con.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        con.setRequestProperty("Authorization", "Bearer " + access);

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        LOGGER.info("Sending 'POST' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        LOGGER.info("Response = " + response.toString());
        return response.toString();

    }
}
